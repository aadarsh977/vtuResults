# getVTUResult
> ***Fork it!***



- When VTU publishes the result, its result page rarely loads and we are forced to reload and wait multiple times.
- This project aims to solve that problem by re-establishing the connection and it keeps on trying until we get result.
- Right now, it can only parse the regular exam results.
- If result is not yet published, then it'll resend request after 60 seconds.

## Getting Started

Install Python >= 3.0 on your local system from https://www.python.org/downloads/ or use the default package manager if you're using Linux distribution.
Python 3.4 and higher ship with 'pip'. If it is not included, install from https://packaging.python.org/installing/#requirements-for-installing-packages

### Prerequisites

- lxml==3.7.3
- requests==2.13.0

Install the prerequisites using:
```
pip install -r requirements.txt
```

### Deployment & Usage
### ResultTypeFlag is optional and should be used if you want raw output!

Clone the repo OR directly download the ZIP file from the project page [Download](https://gitlab.com/aadarsh977/vtuResults/repository/archive.zip?ref=master)
```
git clone git@gitlab.com:aadarsh977/vtuResults.git
```

From the directory:

```
python result.py [ResultTypeFlag] USN
```

Here,

- ResultTypeFlag
-    c = crude; unformatted whole extracted result (optional)
- USN - USN of student

Example:
```
python result.py c 1EW13CS001
python result.py 1EW13CS001
```

## Contributing

- The project is still at its infancy. So, any suggestions, criticisms or contributions are heartly welcome.
- Please report issues. I bet there are plenty of them. :)

## Authors

* **Aadarsha Shrestha** - [aadarsh977](https://gitlab.com/aadarsh977)

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.


## Acknowledgments

* [StackOverFlow](https://www.stackoverflow.com/)
* [VTU](https://results.vtu.ac.in)

