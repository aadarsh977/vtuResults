'''
getBatchResults.py
Author: Aadarsha Shrestha (aaadarsh977@gmail.com)
Purpose: Get result of the range of USN
Input: USN format
Usage: python getBatchResults.py start_usn end_usn

ToDo:
    1. Check year code with current year for more accuracy 
    2. Multithreading for faster result
'''

'''
Get result of the range of USN
Copyright (C) 2017  Aadarsha Shrestha

This file is part of getVTUResults.

    getVTUResults is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    getVTUResults is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with getVTUResults.  If not, see <http://www.gnu.org/licenses/>.
'''

from result import getResultOf
import sys, re

'''
User has to input starting and ending USN.
'''

def getBatchResults():
    # Check for the validity of the arguments
    args = sys.argv
    if (len(args) == 3):
        start_usn = args[1]
        end_usn = args[2]

        # Check validity of USN
        usn_re = re.compile('^[1-4]([A-Z]|[a-z]){2}\d{2}([A-Z]|[a-z]){2}\d{3}$')
        if (usn_re.match(start_usn) and usn_re.match(end_usn)):
            # print(start_usn, end_usn)

            # Get the range
            starting_num = int(start_usn[7:10])
            no_of_results = int(end_usn[7:10])

            # Get results
            usn_skel = start_usn[0:7]
            for usn in range(starting_num, no_of_results + 1):
                usn = usn_skel + str(usn).zfill(3)
                # print(usn)
                getResultOf(usn, True)



        else:
            print('Start or end USN invalid')


if __name__ == "__main__":
   getBatchResults()
