'''
result.py
Author: Aadarsha Shrestha (aaadarsh977@gmail.com)
Purpose: Prints result of the provided USN
Input: USN
Output: Result
'''

'''
Prints result of a single VTU USN
Copyright (C) 2017  Aadarsha Shrestha

This file is part of getVTUResults.

    getVTUResults is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    getVTUResults is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with getVTUResults.  If not, see <http://www.gnu.org/licenses/>.
'''

from lxml import html
import requests, sys, re

# Global
captions = ['Subject Code', 'Subject Name', 'Internal Marks', 'External Marks', 'Total', 'Result', 'Total Marks', 'Division']
'''
    Crude result: result_type_flag = True
    Refined result: result_type_flag = False
'''

result_type_flag = False
usn = ''


# Extract result from parsed Result page
def extract_data(html_tree, result):
    '''
    Result page XPath format till the first table cell(Subject Code):
    /html/body/div/div/div/div[3]/div[1]/div/div[2]/div[2]/div/table/tbody[1]/tr/td[1]
    '''

    print('\tExtracting result data. . .')

    for i in range(1, 9):
        table_row_xpath = '//tbody[' + str(i) + ']/tr/'
        result.append({})

        '''
        Get data from table
        data saved in order: Subject Code, Subject Name, Internal Marks, External Marks, Total, Result
        '''

        for j in range(1, 7):
            subject = result[i - 1]
            cell_value = html_tree.xpath(table_row_xpath + 'td[' + str(j) + ']/text()')
            subject[captions[j - 1]] = cell_value[0]

    total_xpath = './/td[b = "Total Marks"]/following-sibling::td/b'
    data = html_tree.xpath(total_xpath)[0].text.strip(": ")
    result.append({})
    result[8][captions[6]] = data
    division_xpath = './/td[b = "Result"]/following-sibling::td/b'
    data = html_tree.xpath(division_xpath)[0].text.strip(": ")
    result.append({})
    result[9][captions[7]] = data


# Print the extracted data
def print_data(result_data, captions):
    print('\tPrinting results. . .')
    print('\n--------------------\n')
    print('\tResult:')
    if (result_type_flag):
        for row in result_data:
            print(result_data[row])
    else:
        # print('Refined results here!')
        # print(result_data)

        for row in result_data:
            try:
                # print(row['Subject Code'])
                subject_code = str(row['Subject Code'])
                total = str(row['Total'])
                print('\t' + subject_code + ':     ' + total)

            except KeyError:
                break
        print()
        total_marks = float(result_data[8]['Total Marks'])
        print('\tTotal Marks: ', total_marks)
        print('\tDivision:    ', result_data[9]['Division'])
        print('\tPercentage:  ', total_marks / 900 * 100)
        print('\n--------------------\n')


def getResultOf(usn, batch_result):
    while(1):
        try:
            # Retrieve web page
            print('USN: ' + usn)
            url = 'http://results.vtu.ac.in/results/result_page.php?usn=' + usn
            print('\tGetting Results. . .')
            response = requests.get(url, timeout=30)
        except requests.exceptions.Timeout:
            print('\tTimeOut!\n')
        except requests.exceptions.ConnectionError:
            print('\tConnection Error!\n')

        else:
            data = response.text
            if (data.find('University Seat Number is not available or Invalid..!') != -1):
                print('\tResult not available!\n')
                if (batch_result):
                    return -1
                else:
                    sleep(60)
            else:
                # print(data)

                # Parse the contents
                print('\tParsing contents. . .')
                html_tree = html.fromstring(response.content)

                result = []
                extract_data(html_tree, result)
                print_data(result, captions)
                break

def main():
    # Format URL and pass to method
    arguments = sys.argv

    '''
    Crude result: result_type_flag = True
    Refined result: result_type_flag = False
    '''
    result_type_flag = False
    usn = ''

    # Check for arguments
    # For crude result, flag specified as 'c'
    if (len(arguments) == 3):
        # ResultTypeFlag: Specify 'c' for crude result
        flag = arguments[1]
        if (flag == 'c'):
            # print('Crude result')
            result_type_flag = True
            usn = arguments[2]

    # For refined result, no flag specified
    elif (len(arguments) == 2):
        # print('Refined result')
        usn = str(arguments[1])

    else:
        print('Error!\nFormat: python result.py [c] USN')

    # Check if USN format is valid
    # print(usn)
    if (usn):
        usn_re = re.compile('^[1-4]([A-Z]|[a-z]){2}\d{2}([A-Z]|[a-z]){2}\d{3}$')
        if (usn_re.match(usn)):
            getResultOf(usn, False)
        else:
            print('Enter proper USN!')

if __name__ == "__main__":
   main()
